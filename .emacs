(add-to-list 'load-path "/usr/local/lib/erlang/lib/tools-2.6.6.5/emacs")
(setq erlang-root-dir "/usr/local/lib/erlang/")
(add-to-list 'exec-path "/usr/local/lib/erlang/bin")
(require 'erlang-start)
(require 'erlang-flymake)
(require 'color-theme)

(tabbar-mode t)
(setq x-select-enable-clipboard t)

(setq dired-dwim-target t)
(setq dired-recursive-copies (quote always))
(setq dired-recursive-deletes (quote top))

(setq erlang-indent-guard 4)
(setq erlang-argument-indent 4)
;;(setq erlang-compile-use-outdir "../ebin")
(setq-default indent-tabs-mode nil)

(setq highlight-tabs t)
(setq highlight-trailing-whitespace t)

(put 'dired-find-alternate-file 'disabled nil)
(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(color-theme-selection "Blue Mood" nil (color-theme_seldefcustom))
 '(dired-listing-switches "-alBGgh")
 '(ido-mode (quote file) nil (ido))
 '(inhibit-startup-screen t))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )
